/*
 *  JFreqScopePanel.scala
 *  (ScalaCollider-Swing)
 *
 *  Copyright (c) 2008-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.swing.j

import de.sciss.audiowidgets.AxisFormat
import de.sciss.audiowidgets.j.Axis
import de.sciss.numbers.Implicits._
import de.sciss.synth.Bus

import java.awt.event.{ActionEvent, InputEvent, KeyEvent}
import javax.swing.{AbstractAction, JComponent, JPanel, KeyStroke}
import scala.math.ceil

/** Abstract component to view an oscilloscope for a real-time signal.
  * It builds configurable controls around a central `JScopeView`.
  *
  * @define keyboard
  * The following keyboard shortcuts exist:
  *
  * <ul>
  * <li><kbd>Ctrl</kbd>-<kbd>Up</kbd>/<kdb>Down</kbd>: increase or decrease vertical zoom
  * <li><kbd>Ctrl</kbd>-<kbd>Right</kbd>/<kdb>Left</kbd>: increase or decrease horizontal zoom
  * <li><kbd>Space</kbd>: toggle run/pause
  * <li><kbd>Period</kbd>: pause
  * <li><kbd>J</kbd>/<kbd>L</kbd>: decrease or increase channel offset
  * <li><kbd>Shift</kbd>-<kbd>J</kbd>/<kbd>L</kbd>: decrease or increase number of channels
  * <li><kbd>K</kbd>: switch between audio and control rate buses
  * <li><kbd>I</kbd>/<kbd>O</kbd>: switch to audio inputs and audio outputs
  * <li><kbd>S</kbd>: switch between parallel and overlay mode
  * <li><kbd>Shift</kbd>-<kbd>S</kbd>: switch between Lissajous (X/Y) and normal (X over time) mode
  * </ul>
  */
trait AbstractFreqScopePanel extends AbstractScopePanel[JFreqScopeView] with FreqScopeViewLike {

  private[this] var nyquist = 0.0

  override protected def supportLissajous: Boolean = false

  override protected def supportXLog: Boolean = true

  override protected def bipolar: Boolean = false

  override protected def minXZoom: Float = 1f/256

  override protected def maxXZoom: Float = 1f

  override protected def isLogX: Boolean = logFreq

  override protected def xUnit: String = "Hz"

  override protected def install(parent: JPanel): Unit = {
    super.install(parent)
    // install additional key actions
    val im        = parent.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    val am        = parent.getActionMap
    val ksIncRes = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT , InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK)
    val ksDecRes = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK)
    val aIncRes = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        xResolution = math.min(xResolution * 2f, 16f) // currently, const-q kernel calculation gets incredibly slow beyond this
    }
    val aDecFreq = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        xResolution = math.max(xResolution * 0.5f, 1.0f/32)
    }
    am.put("res-inc", aIncRes)
    im.put(ksIncRes, "res-inc")
    am.put("res-dec", aDecFreq)
    im.put(ksDecRes, "res-dec")

    logFreq = logFreq
    logAmp  = logAmp
  }

  override def xResolution: Float = view.xResolution

  override def xResolution_=(value: Float): Unit = {
    view.xResolution = value
    updateXAxis() // while the visible bounds are not updated, this recalculates the buffer size!
  }

  override def logFreq: Boolean = view.logFreq

  override def logFreq_=(value: Boolean): Unit =
    setLogModeMode(x = value, y = logAmp)

  override def logFreqMin: Float = view.logFreqMin

  override def logFreqMin_=(value: Float): Unit = {
    view.logFreqMin = value
    updateXAxis()
  }

  override def bus_=(value: Bus): Unit = {
    nyquist = value.server.sampleRate * 0.5
    super.bus_=(value)
  }

  override protected def setLogModeModeFromUI(x: Boolean, y: Boolean): Unit = {
    view.logFreq = x
    super.setLogModeModeFromUI(x, y)
  }

  override protected def updateXAxis(a: Axis): Unit = {
    val viewWidth   = view.getWidth
    val bestBufSize = if (channelStyle == 2) {
      // lissajous is not really supported by the spectrum analyzer,
      // so this code is more or less just a remainder from the regular scope
      val maxVal    = 1.0 / xZoom
      val minVal    = -maxVal
      a.minimum     = minVal
      a.maximum     = maxVal
      a.fixedBounds = maxVal >= 0.5
      a.format      = AxisFormat.Decimal
      ceil(viewWidth * 4 * xResolution).toInt
    } else {
      val numFramesF = viewWidth * xResolution / xZoom
      ceil(numFramesF).toInt
    }

    if (!isBufSizeSet) {
      if (bestBufSize > 65536) println(s"BIG: $bestBufSize")
      // we allow a slight 'under-sampling' (94%), so that if for example
      // the view width is 540 pixels, we still run 512 frames instead of 1024.
      val bestBufSizeP = (bestBufSize * 0.94f).toInt.clip(64, 65536).nextPowerOfTwo
      setBufferSize(bestBufSizeP)
    }

    if (channelStyle != 2) {
      // val numFramesF = viewWidth * xZoom / freqZoom
      val f = xZoom // xZoom / freqZoom // numFramesF / bufferSize
      val _log      = logFreq
      a.minimum     = if (_log) logFreqMin else 0.0
      a.maximum     = nyquist * f // * 0.01
//      println(s"maximum ${a.maximum}")
      a.fixedBounds = false
      a.logarithmic = _log
      a.format      = if (_log) AxisFormat.Decimal else AxisFormat.Integer
    }
  }
}

/** A component to view an oscilloscope for a real-time signal.
  *
  * $keyboard
  */
class JFreqScopePanel extends JPanel with ScopePanelBase[JFreqScopeView] with AbstractFreqScopePanel {
  private[this] val _view = new JFreqScopeView

  override def view: JFreqScopeView = _view

  install(this)
}