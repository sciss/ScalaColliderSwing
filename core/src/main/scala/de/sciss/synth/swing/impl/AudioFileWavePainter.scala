/*
 *  AudioFileWavePainter.scala
 *  (ScalaCollider-Swing)
 *
 *  Copyright (c) 2008-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth
package swing
package impl

import de.sciss.audiofile.AudioFile.Frames
import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType}
import de.sciss.audiowidgets.j.WavePainter
import de.sciss.audiowidgets.j.WavePainter.{Decimator, MultiResolution}
import de.sciss.audiowidgets.{Axis, AxisFormat}
import de.sciss.file.File
import de.sciss.synth.Import._

import java.awt.event.MouseEvent
import java.awt.{Color, Dimension, Font, Graphics, Graphics2D, Point, RenderingHints}
import java.io.Closeable
import javax.swing.JComponent
import javax.swing.event.MouseInputAdapter
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.concurrent.{ExecutionContext, Future, blocking}
import scala.swing.{BorderPanel, BoxPanel, Component, Frame, Orientation, Swing}
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

// XXX TODO DRY with AudioWidgets
object AudioFileWavePainter {
  private class FileReader(f: File, val af: AudioFile, val decimationFactor: Int, val tupleSize: Int,
                           deleteOnClose: Boolean)
    extends MultiResolution.Reader {

    // println(this)

    override def toString: String =
      s"FileReader($af, $decimationFactor, $tupleSize)${hashCode().toHexString}"

    def close(): Unit = {
      af.cleanUp()
      if (deleteOnClose) {
        f.delete()
        ()
      }
    }

    override def available(sourceOffset: Long, len: Int): Vec[Int] = Vector(0, len)

    override def read(buf: Array[Array[Double]], bufOff: Int, srcOff: Long, len: Int): Boolean = {
      // println(s"$this.read(buf, $bufOff, $srcOff, $len)")
      val bufOffT = bufOff * tupleSize
      val srcOffT = srcOff * tupleSize
      val afRem   = af.numFrames - srcOffT
      val lenT    = len * tupleSize
      if (afRem < 0) return false
      af.seek(srcOffT)
      af.read(buf, bufOffT, lenT)
      true
    }
  }

  private final class WrapImpl(val numChannels: Int, val numFrames: Long, _readers: Vec[FileReader])
    extends MultiResolution.Source with Closeable {

    def readers: Vec[MultiResolution.Reader] = _readers

    override def close(): Unit = _readers.foreach(_.close())

    override def toString = s"MultiResolution.Source(numChannels = $numChannels, numFrames = $numFrames)@${hashCode().toHexString}"
  }

  // bufSize: 'nominal', i.e. without factor of tuple-out-size
  private class Decimation(val dec: Decimator, val af: AudioFile, val bufSize: Int) {
    var bufOff  = 0
    val buf: Frames = af.buffer(bufSize * dec.tupleOutSize)

    def bufRem  : Int = bufSize  - bufOff
  }

  def apply(file: File): Frame = {
    val afFull: AudioFile = AudioFile.openRead(file)
    val sr          = afFull.sampleRate
    val numFr       = afFull.numFrames  // math.ceil(duration * sr).toInt
    val numCh       = afFull.numChannels
    var disposed    = false
    var funCleanUp  = () => disposed = true

    import WavePainter.MultiResolution

    val fontWait    = new Font(Font.SANS_SERIF, Font.PLAIN, 24)
    var paintFun: Graphics2D => Unit = { g =>
      g.setFont(fontWait)
      g.setColor(Color.white)
      g.drawString("\u231B ...", 10, 26) // u231A = 'watch', u231B = 'hourglass'
    }

    val ggWave = new JComponent {
      setFocusable(true)
      setPreferredSize(new Dimension(400, 400))

      override def paintComponent(g: Graphics): Unit = {
        val g2 = g.asInstanceOf[Graphics2D]
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING  , RenderingHints.VALUE_ANTIALIAS_ON)
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE )
        g2.setColor(Color.black)
        g2.fillRect(0, 0, getWidth, getHeight)
        paintFun(g2)
      }
    }

    val ggAxisH     = new Axis(Orientation.Horizontal)
    ggAxisH.format  = AxisFormat.Integer
    val ggAxisV     = new Axis(Orientation.Vertical  )
    ggAxisV.format  = AxisFormat.Integer

    val box = new BorderPanel {
      add(Component.wrap(ggWave), BorderPanel.Position.Center)
      add(new BoxPanel(Orientation.Horizontal) {
        contents += Swing.HStrut(ggAxisV.preferredSize.width)
        contents += ggAxisH
      }, BorderPanel.Position.North)
      add(ggAxisV, BorderPanel.Position.West )
    }

    val f   = GUI.makeFrame("Wave", "PlotFrame", box /* , smallBar = false */) {
      funCleanUp()
    }

    def createSource(): MultiResolution.Source with Closeable = {
      /*
          - generate the sequence of decimators
          - for each decimator, allocate a temporary audio file, and a decimation buffer

       */

      // val baseSize = numFr * numCh * 4  // base bytes
      val funSq   = Decimator.suggest(numFr)
      val numDec  = funSq.length
      val decSq   = new Array[Decimation](numDec + 1)
      val dec0    = new Decimation(Decimator.dummy, afFull, 8192)
      val readersB = Vector.newBuilder[FileReader]
      readersB.sizeHint(numDec + 1)
      decSq(0)    = dec0
      readersB += new FileReader(file, afFull, 1, 1, deleteOnClose = false)

      try {
        var di = 0
        var fTot = 1
        while (di < numDec) {
          // val fileSize: Long = baseSize * d.tupleOutSize / d.factor
          // val needsW64 = fileSize > 0x7FFFFFFF
          val fSub  = File.createTemp("scalacollider", ".irc" /* if (needsW64) ".w64" else ".aif"*/)
          val d     = funSq(di)
          val srSub = sr / d.factor
          val spSub = AudioFileSpec(AudioFileType.IRCAM, numChannels = numCh, sampleRate = srSub)
          val afSub = AudioFile.openWrite(fSub, spSub)
          val bfSz  = {
            val n   = d.factor // * d.tupleOutSize
            val dv  = (8192 + n - 1) / n
            dv * n
          }
          di += 1
          decSq(di) = new Decimation(d, afSub, bfSz)
          fTot *= d.factor
          readersB += new FileReader(fSub, afSub, decimationFactor = fTot, tupleSize = d.tupleOutSize,
            deleteOnClose = true)
        }

        if (numDec > 0) {
          var remFull = numFr
          while (remFull > 0L) {
            val inLen = math.min(dec0.bufRem, remFull).toInt
            remFull -= inLen
            // val flush = remFull == 0L
            afFull.read(dec0.buf, dec0.bufOff, inLen)
            dec0.bufOff += inLen
            var di = 0
            var decPre = dec0
            while (di < numDec) {
              di += 1
              val decSuc  = decSq(di)
              val d       = decSuc.dec
              val inLenD  = decPre.bufOff / d.factor
              val outLen  = math.min(decSuc.bufRem, inLenD)
              if (outLen > 0) {
                val inLenC = outLen * d.factor
                val cpyOff = inLenC * d.tupleInSize
                val cpyLen = (decPre.bufSize - inLenC) * d.tupleInSize
                var ch = 0
                while (ch < numCh) {
                  val bufPre = decPre.buf(ch)
                  val bufSuc = decSuc.buf(ch)
                  /*
                      |----------------------|
                      RRRRRRRRRRRR
                      WWWWWWWW
                   */
                  decSuc.dec.decimate(bufPre, 0, bufSuc, decSuc.bufOff, outLen)
                  if (cpyLen > 0) System.arraycopy(bufPre, cpyOff, bufPre, 0, cpyLen)
                  ch += 1
                }
                decSuc.af.write(decSuc.buf, decSuc.bufOff * d.tupleOutSize, outLen * d.tupleOutSize)
                decPre.bufOff -= inLenC
                decSuc.bufOff += outLen
              }
              decPre = decSuc
            }
          }
        }
      } catch {
        case NonFatal(ex) =>
          decSq.foreach { dec => if (dec != null) dec.af.cleanUp() }
          throw ex
      }

      val readers = readersB.result()
      new WrapImpl(numCh, numFr, readers)
    }

    def openBuffer(pntSrc: MultiResolution.Source with Closeable): Unit = {
      funCleanUp = () => pntSrc.close()

      val display = new WavePainter.Display {
        def numChannels : Int   = numCh
        def numFrames   : Long  = numFr

        def refreshAllChannels(): Unit = ggWave.repaint()

        def channelDimension(result: Dimension): Unit = {
          result.width  = ggWave.getWidth
          val h         = ggWave.getHeight
          result.height = (h - ((numCh - 1) * 4)) / numCh
        }

        def channelLocation(ch: Int, result: Point): Unit = {
          result.x        = 0
          val h           = ggWave.getHeight
          val viewHeight  = (h - ((numCh - 1) * 4)) / numCh
          val trackHeight = viewHeight + 4
          result.y        = trackHeight * ch
        }
      }

      val painter = MultiResolution(pntSrc, display)

      val zoom: WavePainter.HasZoom = new WavePainter.HasZoom {
        def startFrame: Long = painter.startFrame
        def startFrame_=(value: Long): Unit = {
          painter.startFrame  = value
          // println(s"startFrame = $value")
          ggAxisH.minimum     = value.toDouble / sr
        }

        def stopFrame: Long = painter.stopFrame
        def stopFrame_=(value: Long): Unit = {
          painter.stopFrame  = value
          // println(s"stopFrame = $value")
          ggAxisH.maximum    = value.toDouble / sr
        }

        def magLow: Double = painter.magLow
        def magLow_=(value: Double): Unit = {
          painter.magLow  = value
          // println(s"magLow = $value")
          ggAxisV.minimum = value * 100
        }

        def magHigh: Double = painter.magHigh
        def magHigh_=(value: Double): Unit = {
          painter.magHigh = value
          // println(s"magHigh = $value")
          ggAxisV.maximum = value * 100
        }
      }

      zoom.startFrame     = 0L
      zoom.stopFrame      = numFr
      zoom.magLow         = -1d
      zoom.magHigh        = 1d
      painter.peakColor   = Color.gray
      painter.rmsColor    = Color.white
      paintFun            = painter.paint
      WavePainter.HasZoom.defaultKeyActions(zoom, display).foreach(_.install(ggWave))
      ggWave.addMouseWheelListener(WavePainter.HasZoom.defaultMouseWheelAction(zoom, display))
      ggWave.repaint()
      ggWave.requestFocus()

      val mia: MouseInputAdapter = new MouseInputAdapter {
        private def frame(e: MouseEvent): Long = {
          val w    = ggWave.getWidth
          val clip = e.getX.clip(0, w)
          // val f = clip.linLin(0, w, zoom.startFrame, zoom.stopFrame)
          val f = clip.linLin(0.0, w, viewStart.toDouble, (viewStart + viewSpan).toDouble)
          (f + 0.5).toLong
        }

        private var dragStart = 0L
        private var viewStart = 0L
        private var viewSpan  = 0L

        override def mousePressed(e: MouseEvent): Unit = {
          viewStart = zoom.startFrame
          viewSpan  = zoom.stopFrame - viewStart
          dragStart = frame(e)
        }

        override def mouseDragged(e: MouseEvent): Unit = {
          val dragStop    = frame(e)
          val delta       = dragStart - dragStop
          val newStart    = math.max(0L, math.min(numFr - viewSpan, viewStart + delta))
          zoom.startFrame = newStart
          zoom.stopFrame  = newStart + viewSpan
          ggWave.repaint()
        }
      }
      ggWave.addMouseListener      (mia)
      ggWave.addMouseMotionListener(mia)
      ggWave.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.HAND_CURSOR))
    }

    import ExecutionContext.Implicits.global

    val futSrc = Future {
      blocking {
        createSource()
      }
    }

    futSrc.onComplete {
      case Success(pntSrc) =>
        Swing.onEDT {
          // println(s"disposed $disposed")
          if (disposed) {
            pntSrc.close()
          } else {
            openBuffer(pntSrc)
          }
        }

      case Failure(ex) =>
        ex.printStackTrace()
    }

    f
  }
}