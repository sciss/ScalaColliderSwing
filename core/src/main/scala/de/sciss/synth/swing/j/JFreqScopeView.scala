/*
 *  JScopeView2.scala
 *  (ScalaCollider-Swing)
 *
 *  Copyright (c) 2008-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.swing.j

import de.sciss.dsp.{ConstQ, FastLog, Threading, Window}
import de.sciss.numbers.Implicits.floatNumberWrapper
import de.sciss.synth.swing.impl.ScopeViewImpl
import de.sciss.transform4s.fft.DoubleFFT_1D

import java.util
import scala.util.control.NonFatal

trait FreqScopeViewLike extends ScopeViewLike {
  /** Determines the over- or under-sampling
    * to achieve higher or lower resolution spectra.
    *
    * At the default of `1.0`, the resolution matches
    * the screen resolution (it will be slightly higher,
    * as it is rounded up to a power of two). At a value
    * of `2.0`, the spectra will have twice the resolution
    * (data per frequency band), at a value of `0.5`, the
    * spectra will have half the resolution. The temporal
    * resolution is reciprocal, so higher values will lead
    * to longer time windows and thus lower update rates.
    */
  def xResolution: Float

  /** Determines the over- or under-sampling
    * to achieve higher or lower resolution spectra.
    *
    * At the default of `1.0`, the resolution matches
    * the screen resolution (it will be slightly higher,
    * as it is rounded up to a power of two). At a value
    * of `2.0`, the spectra will have twice the resolution
    * (data per frequency band), at a value of `0.5`, the
    * spectra will have half the resolution. The temporal
    * resolution is reciprocal, so higher values will lead
    * to longer time windows and thus lower update rates.
    */
  def xResolution_=(value: Float): Unit

  /** Whether the frequency axis is logarithmic */
  def logFreq: Boolean

  /** Whether the frequency axis is logarithmic */
  def logFreq_=(value: Boolean): Unit

  /** The lowest displayed frequency in Hertz, when `logFreq` is enabled. */
  def logFreqMin: Float

  /** The lowest displayed frequency in Hertz, when `logFreq` is enabled. */
  def logFreqMin_=(value: Float): Unit
}

object JFreqScopeView {
  private[j] class Data(frames: Int, sampleRate: Double, logFreqMin: Float) {
    //    private[this] val frames    = _state config.useFrames << 1 // config.vecFrames
    lazy val fft1d  : DoubleFFT_1D  = DoubleFFT_1D(frames)
    val vector  : Array[Double] = new Array[Double](frames)
    // cf. https://math.stackexchange.com/questions/4273829/
    private[this] val logBase = Math.pow(10.0,1.0/10.0)
    lazy val magLog: FastLog    = FastLog(logBase /* 10.0 */, q = 11)
    lazy val constQ: ConstQ = {
      val cfg = ConstQ.Config()
      // numKernels = math.ceil(bandsPerOct * Util.log2(maxFreq / minFreq)).toInt := frames/2
      val numKernels  = frames/2
      val nyquist     = sampleRate/2
      val maxFreq     = nyquist
      val minFreq     = logFreqMin // 20.0
//      val minFreq0    = 30.0 // 20.0
//      val numOct      = Util.log2(maxFreq / minFreq0)
//      val perOct0     = numKernels / numOct
//      val perOct      = math.round(perOct0).toInt
//      println(s"numOct $numOct, perOct0 $perOct0")
//      val minFreq     = maxFreq / math.exp(numKernels / perOct.toDouble * Util.Ln2)
//      println(s"minFreq $minFreq")
//      cfg.bandsPerOct = perOct
      cfg.minFreq     = minFreq
      cfg.maxFreq     = maxFreq
      cfg.numKernels  = numKernels
      cfg.maxTimeRes  = 8.0
      cfg.energyWeighted = false
      cfg.maxFFTSize  = frames // math.min(8192, frames)
      cfg.sampleRate  = sampleRate
      cfg.threading   = Threading.Single
      val res = ConstQ(cfg)
      // println(s"numKernels ${res.numKernels}; minFreq ${res.config.minFreq}, maxFreq ${res.config.maxFreq}, bandsPerOct ${res.config.bandsPerOctF}")
      res
    }
    lazy val constQBuf: Array[Double] = new Array[Double](constQ.numKernels)
    val linLinGain: Double = 8.0 / frames
    val logLinGain: Double = 4.0 / frames
    val linLogGain: Double = 5.6  // this number is beyond good and evil
    val logLogGain: Double = 8.0
    val window  : Array[Double] = Window.Hanning.fill(null, 0, frames)
  }
}
/** $info
  *
  * To have additional user interface controls, `JScopePanel` can be used, which contains a `JScopeView`.
  */
class JFreqScopeView extends ScopeViewImpl[JFreqScopeView.Data] with FreqScopeViewLike {
  private[this] var _xResolution  = 1.0f
  private[this] var _logFreqMin   = 30.0f
  private[this] var _logFreq      = false
  private[this] var _xScaleBase   = 0f  // zero means dirty

  override protected val vecFramesFactor: Int = 2

  // private var DEBUG_COUNT = 0

  override protected def calcPolySize(width: Int): Int = {
    updateXScale()
    Math.ceil(config.useFrames / _xScaleBase).toInt
  }

  override protected def prepareVector(data: JFreqScopeView.Data, vc: Array[Float], n: Int): Unit = {
    val c     = config
    val numCh = c.numChannels
    val ncH   = n / numCh
    val nc    = ncH << 1
    val fftVc = data.vector
    // XXX TODO bad. this can happen because pntVector and scopeData can be briefly out of sync
    if (nc > fftVc.length) return

//    if (DEBUG_PRINT) {
//      println(s"n $n, numCh $numCh, nc $nc, vc.length ${vc.length}, fftVc.length ${fftVc.length}, pnt.length ${DEBUG_PNT_VEC.length}")
//    }

    var ci  = 0
    while (ci < numCh) {
      var i = ci
      var j = 0
      val w = data.window
      while (j < nc) {
        fftVc(j) = vc(i).toDouble * w(j)
        i += numCh
        j += 1
      }
      if (nc < fftVc.length) {
        util.Arrays.fill(fftVc, nc, fftVc.length, 0d)
      }

      if (_logFreq) {
        // println(s"--- in $DEBUG_COUNT")
        val constQ  = try {
          data.constQ
        } catch {
          case NonFatal(ex) =>
            ex.printStackTrace()
            throw ex
        }
        val cqBuf = constQ.transform(fftVc, nc /*fftVc.length*/, data.constQBuf)
        i = ci
        j = 0
        if (logAmp) {
          val log   = data.magLog
          val gain  = data.logLogGain
          while (j < ncH) {
            val magSq   = if (j < cqBuf.length) cqBuf(j) * gain else 0.0
            val magLog  = log.calc(magSq)
            vc(i) = Math.max(-320f, magLog.toFloat)
            j += 1
            i += numCh
          }
        } else {
          val gain = data.linLogGain
          while (j < ncH) {
            val magSq = if (j < cqBuf.length) cqBuf(j) else 0.0
            val mag   = Math.sqrt(magSq) * gain
            vc(i) = mag.toFloat
            j += 1
            i += numCh
          }
        }
        // println(s"--- out $DEBUG_COUNT")
        // DEBUG_COUNT += 1

      } else {
        data.fft1d.realForward(fftVc)
        i = ci
        j = 0
        fftVc(1) = 0.0  // DC is real only
        if (logAmp) {
          val log   = data.magLog
          val gain  = data.logLinGain
          while (j < nc) {
            val re      = fftVc(j) * gain; j += 1
            val im      = fftVc(j) * gain; j += 1
            val magSq   = re * re + im * im
            val magLog  = log.calc(magSq)
            vc(i) = Math.max(-320f, magLog.toFloat)
            i += numCh
          }
        } else {
          val gain = data.linLinGain
          while (j < nc) {
            val re      = fftVc(j); j += 1
            val im      = fftVc(j); j += 1
            val magSq   = re * re + im * im
            val mag     = Math.sqrt(magSq) * gain
            vc(i) = mag.toFloat
            i += numCh
          }
        }
      }

      // println(f"PEAK $PEAK%1.1f dB at $PEAK_BIN / ${w.length/2}")
      ci += 1
    }
  }

  override protected def createScopeData(polySize: Int): JFreqScopeView.Data =
    new JFreqScopeView.Data(polySize /*value.useFrames << 1*/, sampleRate = config.server.sampleRate,
      logFreqMin = logFreqMin)

//  private var DEBUG_PRINT = false
//
//  addMouseListener(new java.awt.event.MouseAdapter {
//    override def mousePressed(e: java.awt.event.MouseEvent): Unit = DEBUG_PRINT = true
//  })

  override def xZoom_=(value: Float): Unit = {
    _xScaleBase = 0f  // dirty
    super.xZoom_=(value)
  }

  private def updateXScale(): Unit =
    if (_xScaleBase == 0f) {  // dirty
      _xScaleBase = if (logFreq) {
        val nyquist = config.server.sampleRate.toFloat/2
        val maxFreq = xZoom * nyquist
        1f / maxFreq.expLin(logFreqMin, nyquist, 0f, 1f)
        // println(s"maxFreq $maxFreq, xScaleBase ${_xScaleBase}")
      } else {
        1f / xZoom
      }
    }

  override protected def updatePaintData(width: Int, height: Int, heightCh: Int, numChannels: Int): Unit = {
    val f = width.toFloat / config.useFrames
    pntScaleX = _xScaleBase * 4 * f
//    if (DEBUG_PRINT) {
//      DEBUG_PRINT = false
//      println(s"width $width, useFrames ${config.useFrames}, xResolution $xResolution, xZoom $xZoom")
//    }
    updateXScale()

    if (logAmp) {
      pntScaleY = (height << 2) / logAmpMin / numChannels
      // pntOffYIn = yZoom.ampDb
      pntOffYCh = -heightCh
    } else {
      pntScaleY = -(height << 1) * yZoom / numChannels
      // pntOffYIn = 0f
      pntOffYCh = 0 // -heightCh
    }
  }

  override def xResolution_=(value: Float): Unit =
    if (_xResolution != value) {
      _xResolution = value
      markPaintRecalculation()
      repaint()
    }

  override def xResolution: Float = _xResolution

  override def logFreq: Boolean = _logFreq

  override def logFreq_=(value: Boolean): Unit =
    if (_logFreq != value) {
      _logFreq = value
      markPaintRecalculation()
      repaint()
    }

  override def logFreqMin: Float = _logFreqMin

  override def logFreqMin_=(value: Float): Unit =
    if (_logFreqMin != value) {
      _logFreqMin = value
      _xScaleBase = 0f  // dirty
      markPaintRecalculation()
      repaint()
    }

  // constructor
  {
    logFreq = true
    logAmp  = true
  }
}