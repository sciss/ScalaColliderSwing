/*
 *  JScopePanel.scala
 *  (ScalaCollider-Swing)
 *
 *  Copyright (c) 2008-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.swing.j

import de.sciss.audiowidgets.AxisFormat
import de.sciss.audiowidgets.j.Axis
import de.sciss.numbers.Implicits._
import de.sciss.osc
import de.sciss.synth.swing.GUI
import de.sciss.synth.swing.impl.ScopeViewImpl
import de.sciss.synth.swing.j.JScopeView.Config
import de.sciss.synth.{AddAction, AudioBus, Buffer, Bus, ControlBus, GraphFunction, Group, Ops, Synth, addToTail, audio}

import java.awt.event.{ActionEvent, ComponentAdapter, ComponentEvent, InputEvent, ItemEvent, ItemListener, KeyEvent, MouseEvent}
import java.awt.{BorderLayout, Color, Cursor, Graphics2D}
import javax.swing.event.{AncestorEvent, AncestorListener, ChangeEvent, ChangeListener, MouseInputAdapter}
import javax.swing.{AbstractAction, Box, BoxLayout, JComboBox, JComponent, JPanel, JSpinner, KeyStroke, SpinnerNumberModel, SwingConstants}
import scala.collection.immutable.{Seq => ISeq}
import scala.math.{ceil, max, min}
import scala.swing.Swing
import scala.util.control.NonFatal

/** Abstract component to view an oscilloscope for a real-time signal.
  * It builds configurable controls around a central `JScopeView`.
  *
  * @define keyboard
  * The following keyboard shortcuts exist:
  *
  * <ul>
  * <li><kbd>Ctrl</kbd>-<kbd>Up</kbd>/<kdb>Down</kbd>: increase or decrease vertical zoom
  * <li><kbd>Ctrl</kbd>-<kbd>Right</kbd>/<kdb>Left</kbd>: increase or decrease horizontal zoom
  * <li><kbd>Space</kbd>: toggle run/pause
  * <li><kbd>Period</kbd>: pause
  * <li><kbd>J</kbd>/<kbd>L</kbd>: decrease or increase channel offset
  * <li><kbd>Shift</kbd>-<kbd>J</kbd>/<kbd>L</kbd>: decrease or increase number of channels
  * <li><kbd>K</kbd>: switch between audio and control rate buses
  * <li><kbd>I</kbd>/<kbd>O</kbd>: switch to audio inputs and audio outputs
  * <li><kbd>S</kbd>: switch between parallel and overlay mode
  * <li><kbd>Shift</kbd>-<kbd>S</kbd>: switch between Lissajous (X/Y) and normal (X over time) mode
  * </ul>
  */
trait AbstractScopePanel[V <: ScopeViewImpl[_]] extends ScopeViewLike {
  // ---- abstract ----

  def view: V

  protected def mkBusSynth(b: Bus): Unit

  // ---- overridable defaults ----

  protected def supportLissajous: Boolean = true

  protected def supportXLog: Boolean = false

  protected def isLogX: Boolean = false

  protected def bipolar: Boolean = true

  protected def minXZoom: Float = 1f/256

  protected def maxXZoom: Float = 256f

  protected def minYZoom: Float = 1f/32768

  protected def maxYZoom: Float = 32768f

  // protected def init(): Unit = ()

  // ---- impl ----

  private[this] val ggBusType     = {
    val res = new JComboBox(Array("Audio In", "Audio Out", "Audio Bus", "Control Bus"))
    res.setToolTipText("Bus")
    res
  }
  private[this] val mBusOff       = new SpinnerNumberModel(0, 0, 8192, 1)
  private[this] val mBusNumCh     = new SpinnerNumberModel(1, 0 /*1*/, 8192, 1)
  //  private[this] val mBufSize      = new SpinnerNumberModel(4096, 32, 65536, 1)
  private[this] val ggBusOff      = {
    val res = new JSpinner(mBusOff)
    res.setToolTipText("Bus Offset")
    res
  }
  private[this] val ggBusNumCh    = {
    val res = new JSpinner(mBusNumCh)
    res.setToolTipText("No. of Channels")
    res
  }
  //  private[this] val ggBufSize     = new JSpinner(mBufSize)
  private[this] val ggChanStyle = {
    val items = new Array[String](if (supportLissajous) 3 else 2)
    items(0)  = "Parallel"
    items(1)  = "Overlay"
    if (items.length > 2) items(2)  = "Lissajous"
    val res = new JComboBox(items)
    res.addItemListener((_: ItemEvent) => setChannelStyleFromUI(res.getSelectedIndex))
    res.setToolTipText("Channel Style")
    res
  }
  private[this] val ggLogMode = {
    val res = if (supportXLog) {
      val items = new Array[String](4)
      items(0) = s"Lin-Lin"
      items(1) = s"Log-Lin"
      items(2) = s"Lin-Log"
      items(3) = s"Log-Log"
      val _res = new JComboBox(items)
      _res.setToolTipText("Y-X Axes Mode")
      _res
    } else {
      val items = new Array[String](2)
      items(0) = s"Lin"
      items(1) = s"Log"
      val _res = new JComboBox(items)
      _res.setToolTipText("Y-Axis Mode")
      _res
    }
    res.addItemListener((_: ItemEvent) => {
      val i     = res.getSelectedIndex
      val _logY = (i % 2) == 1
      val _logX = (i / 2) == 1
      setLogModeModeFromUI(x = _logX, y = _logY)
    })
    res.setToolTipText(if (supportXLog) "Axes Mode" else "Amplitude Mode")
    res
  }

//  private[this] var startWhenShowing = false  // store 'start' and 'stop' while panel is not yet showing

  private[this] val ggXAxis: Axis = {
    val a = new Axis(SwingConstants.HORIZONTAL)
    a.minimum = 0.0
    a.format  = AxisFormat.Integer
    a.addComponentListener(new ComponentAdapter {
      override def componentResized(e: ComponentEvent): Unit =
        updateXAxis()
    })
    a
  }

  private[this] var ggYAxes = new Array[Axis](0)

  private[this] val pYAxes = {
    val p = new JPanel
    p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS))
    p
  }

  private[this] var _bus        : Bus       = null
  private[this] var _bufSize    : Int       = 4096
  private[this] var _bufSizeSet : Boolean   = false

  protected def isBufSizeSet: Boolean = _bufSizeSet

  private def fix(c: JComponent): c.type = {  // WTF
    c.setMaximumSize(c.getPreferredSize)
    c
  }

  private[this] lazy val pTop1: JPanel = {
    val p = new JPanel
    p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS))
    p.add(fix(ggBusType))
    p.add(fix(ggBusOff))
    p.add(fix(ggBusNumCh))
    //    p.add(Box.createHorizontalStrut(8))
    //    p.add(fix(ggBufSize))
    //    ggBufSize.setToolTipText("Buffer Size")
    p.add(Box.createHorizontalGlue())
    p.add(fix(ggChanStyle))
    p.add(fix(ggLogMode))
    p
  }

  private[this] lazy val pTop2: JPanel = {
    val p = new JPanel
    p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS))
    setNumChannels() // init
    p.add(Box.createHorizontalStrut(ggYAxes(0).getPreferredSize.width))
    p.add(ggXAxis)
    p
  }

  private[this] lazy val pTop: JPanel = {
    val p = new JPanel
    p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS))
    p.add(pTop1)
    p.add(pTop2)
    p
  }

  private[this] val lBusOffNum: ChangeListener = (_: ChangeEvent) =>
    setBusFromUI(mBusOff.getNumber.intValue(), mBusNumCh.getNumber.intValue())

  private[this] val lBusType: ItemListener = (_: ItemEvent) => {
    val t = max(0, ggBusType.getSelectedIndex)
    setBusTypeFromUI(t)
  }

  private[this] var _hover        = false
  private[this] var _hoverText    = ""
//  private[this] var _hoverX       = 0
//  private[this] var _hoverY       = 0

  protected def xUnit: String = ""
  protected def yUnit: String = if (logAmp) "dB" else ""

  private object Mouse extends MouseInputAdapter {
    override def mousePressed(e: MouseEvent): Unit = {
      _hover = true
      view.requestFocus()
      checkHover(e)
    }

    override def mouseMoved   (e: MouseEvent): Unit = checkHover(e)
    override def mouseDragged (e: MouseEvent): Unit = checkHover(e)

    private def checkHover(e: MouseEvent): Unit =
    if (_hover) {
        val xi  = e.getX
        val yi  = e.getY
//        _hoverX = xi
//        _hoverY = yi
        val ax  = ggXAxis
        val xv  = ax.screenToModel(xi)
//        val xs  = ax.formatModelValue(xv)
        val xs  = ax.format.format(xv, decimals = if (ax.format.isInstanceOf[AxisFormat.Integer]) 0 else 1)
        val xu  = xUnit
        val xsu = if (xu.isEmpty) xs else s"$xs $xu"
        val ay  = {
          val sq = ggYAxes
          var ai = 0
          while ({ ai < sq.length && sq(ai).getY < yi }) ai += 1
          ai -= 1
          if (ai >= 0) sq(ai) else null
        }
        val ysu = if (ay == null) "" else {
          val ahm = ay.getHeight - 1
          if (ahm <= 0) "" else {
            val ayi = ahm - (yi - ay.getY)
            val yv  = ay.screenToModel(ayi)
//            val ys  = ay.formatModelValue(yv)
            val ys  = ay.format.format(yv, decimals = if (logAmp) 1 else 3)
            val yu  = yUnit
            if (yu.isEmpty) ys else s"$ys $yu"
          }
        }
        _hoverText = if (ysu.isEmpty) xsu else s"$ysu @ $xsu"
        view.repaint()
      }

    override def mouseReleased(e: MouseEvent): Unit =
      if (_hover) {
        _hover = false
        view.repaint()
      }
  }

  // constructor
  protected def install(parent: JPanel): Unit = {
    parent.setLayout(new BorderLayout(0, 0))
    // install key actions
    val im        = parent.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
    val am        = parent.getActionMap
    val ksIncY    = KeyStroke.getKeyStroke(KeyEvent.VK_UP   , InputEvent.CTRL_DOWN_MASK)
    val ksDecY    = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN , InputEvent.CTRL_DOWN_MASK)
    val ksIncX    = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_DOWN_MASK)
    val ksDecX    = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT , InputEvent.CTRL_DOWN_MASK)
    val ksDecOff  = KeyStroke.getKeyStroke(KeyEvent.VK_J, 0)
    val ksDecNumCh= KeyStroke.getKeyStroke(KeyEvent.VK_J, InputEvent.SHIFT_DOWN_MASK)
    val ksIncOff  = KeyStroke.getKeyStroke(KeyEvent.VK_L, 0)
    val ksIncNumCh= KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.SHIFT_DOWN_MASK)
    val ksSwRate  = KeyStroke.getKeyStroke(KeyEvent.VK_K, 0)
    val ksSwIn    = KeyStroke.getKeyStroke(KeyEvent.VK_I, 0)
    val ksSwOut   = KeyStroke.getKeyStroke(KeyEvent.VK_O, 0)
    val ksSwOver  = KeyStroke.getKeyStroke(KeyEvent.VK_S, 0)
    val ksSwXY    = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK)
    val ksStart   = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0)
    val ksStop    = KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD, 0)

    val aIncY = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        yZoom = (yZoom * 2f).clip(minYZoom, maxYZoom)
    }
    val aDecY = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        yZoom = (yZoom * 0.5f).clip(minYZoom, maxYZoom)
    }
    val aIncX = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        xZoom = (xZoom * (if (channelStyle == 2) 2f else 0.5f)).clip(minXZoom, maxXZoom)
    }
    val aDecX = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        xZoom = (xZoom * (if (channelStyle == 2) 0.5f else 2f)).clip(minXZoom, maxXZoom)
    }
    val aIncOff = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        Option(mBusOff.getNextValue).foreach(mBusOff.setValue)
    }
    val aDecOff = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        Option(mBusOff.getPreviousValue).foreach(mBusOff.setValue)
    }
    val aIncNumCh = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        Option(mBusNumCh.getNextValue).foreach(mBusNumCh.setValue)
    }
    val aDecNumCh = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        Option(mBusNumCh.getPreviousValue).foreach(mBusNumCh.setValue)
    }
    val aSwRate = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit = {
        val newTpe = if (busType < 3) 3 else 2
        setBusTypeFromUI(newTpe)
      }
    }
    val aSwIn = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit = if (_bus != null) {
        val numAudioIn = _bus.server.config.inputBusChannels
        setBusTypeFromUI(0, 0, numAudioIn)
      }
    }
    val aSwOut = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit = if (_bus != null) {
        val numAudioOut = _bus.server.config.outputBusChannels
        setBusTypeFromUI(1, 0, numAudioOut)
      }
    }
    val aSwOver = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        channelStyle = if (channelStyle == 0) 1 else 0
    }
    val aSwXY = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        channelStyle = if (channelStyle == 2) 0 else 2
    }
    val aStart = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        if (isRunning) stop() else start()
    }
    val aStop = new AbstractAction() {
      def actionPerformed(e: ActionEvent): Unit =
        stop()
    }

    am.put("y-inc", aIncY)
    im.put(ksIncY, "y-inc")
    am.put("y-dec", aDecY)
    im.put(ksDecY, "y-dec")
    am.put("x-inc", aIncX)
    im.put(ksIncX, "x-inc")
    am.put("x-dec", aDecX)
    im.put(ksDecX, "x-dec")
    am.put("off-inc", aIncOff)
    im.put(ksIncOff, "off-inc")
    am.put("off-dec", aDecOff)
    im.put(ksDecOff, "off-dec")
    am.put("num-ch-inc", aIncNumCh)
    im.put(ksIncNumCh, "num-ch-inc")
    am.put("num-ch-dec", aDecNumCh)
    im.put(ksDecNumCh, "num-ch-dec")
    am.put("switch-rate", aSwRate)
    im.put(ksSwRate, "switch-rate")
    am.put("switch-in", aSwIn)
    im.put(ksSwIn, "switch-in")
    am.put("switch-out", aSwOut)
    im.put(ksSwOut, "switch-out")
    am.put("switch-over", aSwOver)
    im.put(ksSwOver, "switch-over")
    am.put("switch-xy", aSwXY)
    im.put(ksSwXY, "switch-xy")
    am.put("start", aStart)
    im.put(ksStart, "start")
    am.put("stop", aStop)
    im.put(ksStop, "stop")

    //    am.put("DUMP", new AbstractAction() {
    //      def actionPerformed(e: ActionEvent): Unit = view.DUMP = !view.DUMP
    //    })
    //    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, 0), "DUMP")

    parent.add(pTop  , BorderLayout.NORTH  )
    parent.add(pYAxes, BorderLayout.WEST   )
    parent.add(view  , BorderLayout.CENTER )
    addBusListeners()

    //    ggBufSize.addChangeListener(new ChangeListener {
    //      def stateChanged(e: ChangeEvent): Unit =
    //        bufferSize = mBufSize.getNumber.intValue()
    //    })

    view.overlayPainter = { (g: Graphics2D, _: Int, _: Int) =>
      val paused = !isRunning
      if (paused) {
        g.setColor(GUI.pauseColor)
        g.fillRect( 4, 4, 4, 16)
        g.fillRect(12, 4, 4, 16)
      }
      if (_hover) {
        val fntMt = g.getFontMetrics
        g.setColor(GUI.hoverColor)
//        g.setColor(Util.colrSelection)
//        g.drawString(_hoverText, _hoverX + 6, _hoverY + fntMt.getAscent)
        g.drawString(_hoverText, if (paused) 24 else 4, 4 + fntMt.getAscent)
      }
    }

    view.addAncestorListener(new AncestorListener {
      def ancestorAdded(e: AncestorEvent): Unit = {
        view.requestFocus()
//        if (startWhenShowing) start()
      }

      def ancestorRemoved (e: AncestorEvent): Unit = ()
      def ancestorMoved   (e: AncestorEvent): Unit = ()
    })

    view.addMouseListener       (Mouse)
    view.addMouseMotionListener (Mouse)

    view.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR))

    //    view.addComponentListener(new ComponentAdapter {
    //      override def componentShown(e: ComponentEvent): Unit =
    //        view.requestFocus()
    //    })
  }

  private def setNumChannels(): Unit = {
    val num   = if (channelStyle == 0 && _bus != null) max(1, _bus.numChannels) else 1
    val oldCh = ggYAxes.length
    if (num != oldCh) {
      val axesNew = new Array[Axis](num)
      System.arraycopy(ggYAxes, 0, axesNew, 0, min(num, oldCh))
      if (num < oldCh) {
        var ch = oldCh
        while (ch > num) {
          ch -= 1
          pYAxes.remove(ch)
        }
      } else {
        var ch = oldCh
        while (ch < num) {
          val a = new Axis(SwingConstants.VERTICAL)
          updateYAxis(a)
          axesNew(ch) = a
          pYAxes.add(a)
          ch += 1
        }
      }
      ggYAxes = axesNew
      pYAxes.revalidate()
      pYAxes.repaint()
    }
  }

  private def removeBusListeners(): Unit = {
    ggBusOff  .removeChangeListener (lBusOffNum )
    ggBusNumCh.removeChangeListener (lBusOffNum )
    ggBusType .removeItemListener   (lBusType   )
  }

  private def addBusListeners(): Unit = {
    ggBusOff  .addChangeListener    (lBusOffNum )
    ggBusNumCh.addChangeListener    (lBusOffNum )
    ggBusType .addItemListener      (lBusType   )
  }

  override def channelStyle: Int = view.channelStyle

  override def channelStyle_=(value: Int): Unit = {
    ggChanStyle.setSelectedIndex(value)
    setChannelStyleFromUI(value)
  }

  private def setChannelStyleFromUI(value: Int): Unit = {
    view.channelStyle = value
    if (value == 2 && mBusNumCh.getNumber.intValue() != 2) {
      setBusFromUI(mBusOff.getNumber.intValue(), 2)
    }
    updateXAxis()
    setNumChannels()
  }

  def xZoom: Float = view.xZoom

  def xZoom_=(value: Float): Unit = {
    // println(s"xZoom = $value")
    view.xZoom = value
    updateXAxis()
  }

  override def logAmp: Boolean = view.logAmp

  override def logAmp_=(value: Boolean): Unit =
    setLogModeMode(x = isLogX, y = value)

  protected final def setLogModeMode(x: Boolean, y: Boolean): Unit = {
    val idx = (if (x) 2 else 0) + (if (y) 1 else 0)
    // println(s"setLogModeMode idx $idx")
    ggLogMode.setSelectedIndex(idx)
    setLogModeModeFromUI(x = x, y = y)
  }

  protected def setLogModeModeFromUI(x: Boolean, y: Boolean): Unit = {
    view.logAmp = y
    updateXAxis()
    updateYAxes()
  }

  override def logAmpMin: Float = view.logAmpMin

  override def logAmpMin_=(value: Float): Unit = {
    view.logAmpMin = value
    updateYAxes()
  }

  protected def updateXAxis(): Unit = updateXAxis(ggXAxis)

  protected def updateYAxes(): Unit = {
    val axes = ggYAxes
    var ch = 0
    while (ch < axes.length) {
      val a = axes(ch)
      updateYAxis(a)
      ch += 1
    }
  }

  protected def updateXAxis(a: Axis): Unit = {
    val viewWidth   = view.getWidth
    val bestBufSize = if (channelStyle == 2) {
      val maxVal      = 1.0 / xZoom
      val minVal      = -maxVal
      a.minimum       = minVal
      a.maximum       = maxVal
      a.fixedBounds   = maxVal >= 0.5
      a.format        = AxisFormat.Decimal
      viewWidth * 4
    } else {
      val numFramesF  = viewWidth * xZoom
      a.maximum       = numFramesF
      a.fixedBounds   = false
      a.format        = AxisFormat.Integer
      ceil(numFramesF).toInt
    }

    if (!_bufSizeSet) {
      val bestBufSizeP = bestBufSize.nextPowerOfTwo.clip(64, 65536)
      setBufferSize(bestBufSizeP)
    }
  }

  def yZoom: Float = view.yZoom

  def yZoom_=(value: Float): Unit = {
    // println(s"yZoom = $value")
    view.yZoom = value
    var ch = 0
    while (ch < ggYAxes.length) {
      updateYAxis(ggYAxes(ch))
      ch += 1
    }
  }

  protected def updateYAxis(a: Axis): Unit = {
    if (logAmp) {
      val maxVal    = (1.0 / yZoom).ampDb // log2 * 6
      val minVal    = maxVal + logAmpMin // - 84.0 // 96.0
      a.minimum     = minVal
      a.maximum     = maxVal
      a.fixedBounds = false
    } else {
      val maxVal    = 1.0 / yZoom
      val minVal    = if (bipolar) -maxVal else 0.0
      a.minimum     = minVal
      a.maximum     = maxVal
      a.fixedBounds = maxVal >= 0.5
    }
  }

  def waveColors: ISeq[Color] = view.waveColors

  def waveColors_=(value: ISeq[Color]): Unit =
    view.waveColors = value

  def screenColor: Color = view.screenColor

  def screenColor_=(value: Color): Unit =
    view.screenColor = value

  def start(): Unit = {
//    startWhenShowing = true
//    if (_view.isShowing) {
      view.start()
//    }
  }

  def stop(): Unit = {
//    startWhenShowing = false
    view.stop()
    view.repaint()
  }

  def isRunning: Boolean = /*startWhenShowing ||*/ view.isRunning

  /** The default buffer size is dynamically
    * updated according to the number of frames
    * currently displayed (in Lissajous mode
    * four times the width). If it is set
    * explicitly, the dynamic adjustment is turned off.
    * It can be turned on by setting it to zero.
    */
  def bufferSize: Int = _bufSize

  /** The default buffer size is dynamically
    * updated according to the number of frames
    * currently displayed (in Lissajous mode
    * four times the width). If it is set
    * explicitly, the dynamic adjustment is turned off.
    * It can be turned on by setting it to zero.
    */
  def bufferSize_=(value: Int): Unit =
    if (value > 0) {
      _bufSizeSet = true
      setBufferSize(value)
    } else if (_bufSizeSet) {
      _bufSizeSet = false
      updateXAxis()
    }

  protected def setBufferSize(value: Int): Unit = {
    if (_bufSize != value) {
      require (value > 0)
//      println(s"bufSize ${_bufSize}")
      _bufSize = value
      if (isRunning) bus = bus
    }
  }

  def dispose(): Unit =
    view.dispose()

  private[this] var busType = 0

  private def setBusFromUI(off: Int, num: Int): Unit = if (_bus != null) {
    val b           = _bus
    val s           = b.server
    val numAudioIn  = s.config.inputBusChannels
    val numAudioOut = s.config.outputBusChannels
    val numAudio    = s.config.audioBusChannels
    val numControl  = s.config.controlBusChannels
    var index       = off
    var numChannels = num
    val newBus: Bus = busType match {
      case 0 =>
        index       = min(index, numAudioIn - 1) + numAudioOut
        numChannels = min(numChannels, max(0, numAudioOut + numAudioIn - index))
        AudioBus(s, index, numChannels)

      case 1 =>
        index       = min(index, numAudioOut - 1)
        numChannels = min(numChannels, max(0, numAudioOut - index))
        AudioBus(s, index, numChannels)

      case 2 =>
        index       = min(index, numAudio - 1)
        numChannels = min(numChannels, max(0, numAudio - index))
        AudioBus(s, index, numChannels)

      case 3 =>
        index       = min(index, numControl - 1)
        numChannels = min(numChannels, max(0, numControl - index))
        ControlBus(s, index, numChannels)
    }

    //    println(s"setBusFromUI($off, $num) index $index numChannels $numChannels")

    bus = newBus
  }

  private def setBusTypeFromUI(tpeIdx: Int): Unit =
    setBusTypeFromUI(tpeIdx, mBusOff.getNumber.intValue(), mBusNumCh.getNumber.intValue())

  private def setBusTypeFromUI(tpeIdx: Int, off: Int, num: Int): Unit = if (_bus != null) {
    val oldTpe      = busType
    if (oldTpe == tpeIdx) return
    busType         = max(0, min(3, tpeIdx))

    val b           = _bus
    val s           = b.server
    val numAudioIn  = s.config.inputBusChannels
    val numAudioOut = s.config.outputBusChannels
    val numAudio    = s.config.audioBusChannels
    val numControl  = s.config.controlBusChannels
    var offset      = off
    var numChannels = num

    busType match {
      case 0 if oldTpe == 2 =>
        offset      = max(0, min(offset - numAudioOut, numAudioIn - 1))
        numChannels = min(numChannels, max(0, numAudioIn - offset))
      case 0 =>
        offset      = 0
        numChannels = min(numChannels, numAudioIn)
      case 1 if oldTpe == 2 =>
        offset      = max(0, min(offset, numAudioOut - 1))
        numChannels = min(numChannels, max(0, numAudioOut - offset))
      case 1 =>
        offset      = 0
        numChannels = min(numChannels, numAudioOut)
      case 2 if oldTpe == 0 =>
        offset      = offset + numAudioOut
      case 2 if oldTpe == 1 =>
      // nada
      case 2 =>
        offset      = 0
        numChannels = min(numChannels, numAudio)
      case 3 =>
        offset      = 0
        numChannels = min(numChannels, numControl)
    }

    //    println(s"setBusTypeFromUI($tpeIdx) offset $offset numChannels $numChannels")

    setBusFromUI(off = offset, num = numChannels)
  }

  def bus: Bus = _bus

  protected def mkSynthGraph(b: Bus): Unit = {
    import Ops.stringToControl
    import de.sciss.synth.ugen._
    val inOff = "out".kr
    val buf   = "buf".kr
    if (b.rate == audio) {
      val in = In.ar(inOff, b.numChannels)
      RecordBuf.ar(in, buf = buf)
    } else {
      // XXX TODO --- should use RecordBuf.kr with shorter buffers
      val in = In.kr(inOff, b.numChannels)
      RecordBuf.ar(K2A.ar(in), buf = buf)
    }
    val trFreq  = "freq".kr
    val tr      = Impulse.kr(trFreq)
    val count   = Stepper.kr(tr)
    SendTrig.kr(tr, count)
    ()
  }

  def bus_=(value: Bus): Unit = {
    _bus = value

    val numChannels = value.numChannels
    val s           = bus.server

    mkBusSynth(value)

    val numAudioIn  = s.config.inputBusChannels
    val numAudioOut = s.config.outputBusChannels
    val tpeIdx      = busType

    val (newTpe: Int, offNom: Int, numMax: Int) = value match {
      case ab: AudioBus if tpeIdx != 2 && ab.index >= numAudioOut && ab.index + numChannels <= numAudioOut + numAudioIn =>
        (0, ab.index - numAudioOut, numAudioIn)

      case ab: AudioBus if tpeIdx != 2 && ab.index >= 0 && ab.index + numChannels <= numAudioOut =>
        (1, ab.index, numAudioOut)

      case ab: AudioBus =>
        (2, ab.index, s.config.audioBusChannels)

      case cb: ControlBus =>
        (3, cb.index, s.config.controlBusChannels)
    }

    //    println(s"tpeIdx $tpeIdx index ${value.index} num ${value.numChannels} numAudioOut $numAudioOut numAudioIn $numAudioIn newTpe $newTpe numMax $numMax")

    removeBusListeners()
    ggBusType.setSelectedIndex(newTpe)
    mBusOff.setMaximum(numMax - 1)
    mBusOff.setValue(offNom)
    mBusNumCh.setMaximum(numMax)
    mBusNumCh.setValue(numChannels)
    busType = newTpe
    setNumChannels()
    addBusListeners()
  }
}

trait ScopePanelBase[V <: ScopeViewImpl[_]] extends AbstractScopePanel[V] {
  private[this] var _target     : Group     = null
  private[this] var syn         : Synth     = null
  private[this] var synOnline               = false
  private[this] var _addAction  : AddAction = addToTail

  def target: Group = {
    val _bus = bus
    if (_target != null || _bus == null) _target else _bus.server.rootNode
  }

  override def dispose(): Unit = {
    super.dispose()
    val _syn  = syn
    syn       = null
    if (_syn != null) freeSynth(_syn)
  }

  def target_=(value: Group): Unit =
    _target = value

  def addAction: AddAction = _addAction

  def addAction_=(value: AddAction): Unit = {
    _addAction = value
  }

  protected def mkBusSynth(_bus: Bus): Unit = {
    val numChannels = _bus.numChannels
    val oldSyn      = syn

    if (numChannels > 0) {
      import Ops._
      val gf = new GraphFunction(() => {
        mkSynthGraph(_bus)
      })
      val synDef    = GraphFunction.mkSynthDef(gf)
      val s         = _bus.server
      syn           = Synth (s)
      val b         = Buffer(s)
      val trFreq    = Config.defaultTrigFreq(s)
      val newMsg    = syn.newMsg(synDef.name, target = target,
        args = List("out" -> bus.index, "buf" -> b.id, "freq" -> trFreq),
        addAction = addAction)

      val doneAlloc0  = newMsg :: synDef.freeMsg :: Nil
      val doneAlloc   = if (oldSyn == null) doneAlloc0 else oldSyn.freeMsg :: doneAlloc0
      val useFrames   = bufferSize
      val cfg         = Config.default(s, bufId = b.id, useFrames = useFrames,
        numChannels = numChannels, nodeId = syn.id)
      val bufFrames   = cfg.bufFrames

      val allocMsg  = b.allocMsg(numFrames = bufFrames, numChannels = numChannels, completion =
        Some(osc.Bundle.now(doneAlloc: _*))
      )
      val recvMsg   = synDef.recvMsg
      val syncMsg   = s.syncMsg()
      val synced    = syncMsg.reply

      syn.onEnd { b.free() }

      synOnline = false
      s.!!(osc.Bundle.now(recvMsg, allocMsg, syncMsg)) {
        case `synced` =>
          // println("SYNCED")
          Swing.onEDT {
            synOnline   = true
            view.config = cfg
          }
      }
      ()

    } else {
      if (oldSyn != null) {
        freeSynth(oldSyn)
        syn         = null
        view.config = Config.Empty
      }
    }
  }

  private def freeSynth(syn: Synth): Unit = {
    try {
      val s         = syn.server
      val freeMsg   = syn.freeMsg
      if (synOnline) {
        s ! freeMsg
      } else {
        val syncMsg   = s.syncMsg()
        val synced    = syncMsg.reply
        s.!!(syncMsg) { case `synced` => s ! freeMsg }
        ()
      }
    } catch {
      case NonFatal(_) => // ignore
    }
  }
}

/** A component to view an oscilloscope for a real-time signal.
  *
  * $keyboard
  */
class JScopePanel extends JPanel with ScopePanelBase[JScopeView] {
  private[this] val _view = new JScopeView

  override def view: JScopeView = _view

  install(this)
}
