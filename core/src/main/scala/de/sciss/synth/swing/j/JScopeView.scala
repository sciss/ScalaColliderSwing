/*
 *  JScopeView2.scala
 *  (ScalaCollider-Swing)
 *
 *  Copyright (c) 2008-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth.swing.j

import de.sciss.dsp.FastLog
import de.sciss.synth.Server
import de.sciss.synth.swing.impl.ScopeViewImpl
import de.sciss.synth.swing.impl.ScopeViewImpl.SLICE_SIZE

import java.awt.{Color, Graphics2D}
import scala.collection.immutable.{Seq => ISeq}


trait ScopeViewOverlayPainter {
  def paintScopeOverlay(g: Graphics2D, width: Int, height: Int): Unit
}

/** @define info
  * An oscilloscope canvas component.
  * Has controls for resolution (zoom), setting
  * waveform color, and choosing between three
  * different styles: 0 - parallel, 1 - overlay, 2 - lissajous (x/y).
  */
trait ScopeViewLike {
  /** The drawing style can be one of
    * `0` (or `JScopeView.STYLE_PARALLEL`),
    * `1` (or `JScopeView.STYLE_OVERLAY`),
    * `2` (or `JScopeView.STYLE_LISSAJOUS`).
    *
    * In parallel or "normal" style, each channel is drawn separately
    * in a vertical per-channel arrangement. In overlay mode, all channels
    * are drawn superimposed on each other. In Lissajous or X/Y style,
    * the first channel specifies the x-coordinate, and the second channel
    * specifies the y-coordinate.
    */
  def channelStyle: Int

  /** The drawing style can be one of
    * `0` (or `JScopeView.STYLE_PARALLEL`),
    * `1` (or `JScopeView.STYLE_OVERLAY`),
    * `2` (or `JScopeView.STYLE_LISSAJOUS`).
    *
    * In parallel or "normal" style, each channel is drawn separately
    * in a vertical per-channel arrangement. In overlay mode, all channels
    * are drawn superimposed on each other. In Lissajous or X/Y style,
    * the first channel specifies the x-coordinate, and the second channel
    * specifies the y-coordinate.
    */
  def channelStyle_=(value: Int): Unit

  /** The horizontal zoom factor, depending on the type of scope
    * applied to the time or frequency axis.
    * Smaller values means to "zoom in", therefore _decreasing_
    * the visible density, and vice versa.
    */
  def xZoom: Float

  /** Returns the current horizontal zoom factor. */
  def xZoom_=(value: Float): Unit

  /** Whether the amplitude axis is logarithmic (decibels) */
  def logAmp: Boolean

  /** Whether the amplitude axis is logarithmic (decibels) */
  def logAmp_=(value: Boolean): Unit

  /** The lowest displayed amplitude in decibels, when `logAmp` is enabled. */
  def logAmpMin: Float

  /** The lowest displayed amplitude in decibels, when `logAmp` is enabled. */
  def logAmpMin_=(value: Float): Unit

  /** The vertical zoom factor, usually applied to amplitudes.
    * Smaller values means to "zoom in", therefore _decreasing_
    * the visible density, and vice versa.
    */
  def yZoom: Float

  /** Returns the current vertical zoom factor. */
  def yZoom_=(value: Float): Unit

  def waveColors: ISeq[Color]
  def waveColors_=(value: ISeq[Color]): Unit

  def screenColor: Color
  def screenColor_=(value: Color): Unit

  def start(): Unit
  def stop (): Unit

  def dispose(): Unit

  def isRunning: Boolean
}

object JScopeView {
  final val STYLE_PARALLEL  = 0
  final val STYLE_OVERLAY   = 1
  final val STYLE_LISSAJOUS = 2

  object Config {
    final val Empty = Config(null, -1, 0, 0, 0, -1, 0)

    def defaultTrigFrames(server: Server): Int = {
      val sr      = server.sampleRate
      val fr0     = (sr / 30).toInt
      val bSz     = server.config.blockSize
      val blocks  = (fr0 + bSz - 1) / bSz
      blocks * bSz
    }

    def defaultTrigFreq(server: Server): Double = {
      val frames = defaultTrigFrames(server)
      server.sampleRate / frames
    }

    def default(server: Server, bufId: Int, useFrames: Int, numChannels: Int, nodeId: Int): Config = {
      val SLICE_SIZE_2M = SLICE_SIZE - 1
      val useSize       = useFrames * numChannels
      val slicesPerUse  = (useSize + SLICE_SIZE_2M) / SLICE_SIZE
      val latencyFrames = (server.sampleRate * server.clientConfig.latency).toInt
      val latencyPerUse = latencyFrames * slicesPerUse
      val size0         = Math.max(latencyPerUse, useSize << 1)
      val numSlices     = (size0 * numChannels + SLICE_SIZE_2M) / SLICE_SIZE
      val bufSize       = numSlices * SLICE_SIZE
      val bufFrames     = bufSize / numChannels
      val trigFrames    = defaultTrigFrames(server)
      Config(server, bufId = bufId, bufFrames = bufFrames, numChannels = numChannels,
        useFrames = useFrames, nodeId = nodeId, trigFrames = trigFrames)
    }
  }
  final case class Config(server: Server, bufId: Int, bufFrames: Int, numChannels: Int, useFrames: Int, nodeId: Int,
                          trigFrames: Int) {
    def isEmpty : Boolean = bufId <  0
    def nonEmpty: Boolean = bufId >= 0

    val bufSize   : Int = bufFrames  * numChannels
    val useSize   : Int = useFrames  * numChannels
    val trigSize  : Int = trigFrames * numChannels
  }

  private[j] class Data {
    private[this] val logBase = Math.pow(10.0,1.0/10.0)
    lazy val magLog: FastLog = FastLog(logBase /*10.0*/, q = 11)
  }
}

/** $info
  *
  * To have additional user interface controls, `JScopePanel` can be used, which contains a `JScopeView`.
  */
class JScopeView extends ScopeViewImpl[JScopeView.Data] with ScopeViewLike {

  override protected val vecFramesFactor = 1

  override protected def prepareVector(data: JScopeView.Data, vc: Array[Float], n: Int): Unit =
    if (logAmp) {
      val log = data.magLog
      var i = 0
      while (i < n) {
        val re      = vc(i)
        val mag     = re * re
        val magLog  = log.calc(mag)
        vc(i)       = Math.max(-320f, magLog.toFloat)
        i += 1
      }
    }

  override protected def createScopeData(polySize: Int): JScopeView.Data = new JScopeView.Data

  /** Subclasses should update `pntScaleX`, `pntScaleY`, `pntOffYIn`, `pntOffYCh`.
    *
    * @param height      total painting height in pixels
    * @param heightCh    painting height per channel in pixels
    * @param numChannels effectively painted number of channels
    */
  override protected def updatePaintData(width: Int, height: Int, heightCh: Int, numChannels: Int): Unit = {
    val _lissajous = channelStyle == JScopeView.STYLE_LISSAJOUS
    pntScaleX = if (_lissajous) {
      val offX  = width << 1
      offX * xZoom
    } else {
      4 / xZoom
    }
    if (logAmp) {
      pntScaleY = (height << 2) / logAmpMin / numChannels
      // pntOffYIn = yZoom.ampDb
      pntOffYCh = -heightCh
    } else {
      pntScaleY = -(height << 1) * yZoom / numChannels
      // pntOffYIn = 0f
      pntOffYCh = -heightCh/2
    }
  }
}