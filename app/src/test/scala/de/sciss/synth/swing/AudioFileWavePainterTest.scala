package de.sciss.synth.swing

import de.sciss.submin.Submin
import de.sciss.synth.swing.impl.AudioFileWavePainter

import java.io.File
import scala.swing.SwingApplication

object AudioFileWavePainterTest extends SwingApplication {
  override def startup(args: Array[String]): Unit = {
    Submin.install(true)
    val f = new File(args.headOption.getOrElse(sys.error("Must specify audio file path as argument")))
    AudioFileWavePainter(f)
  }
}
